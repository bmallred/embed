# embed: Utility to bundle files with a Go binary

The intention of this utility is to bundle asset files with a Go binary. Primary focus has been in its use with games created in Go.

## The nature of this document

This program was converted to **literate programming** with the help of [lmt](https://github.com/driusan/lmt) by **Dave MacFarlane**.

To build from this document you may do the following:

``` shell
lmt README.md;
gofmt -w *.go;
```

## Handling arguments

First, we'll need to import in some packages for this to work:

``` go "imports" +=
"fmt"
"os"
```

Then we make sure the minimum amount of arguments are passed to the program

``` go "handling arguments"
if len(os.Args) < 2 {
	fmt.Printf("Not enough arguments provided")
	return
}
```

The first argument, at the index of `1`, represents the base asset directory. `directory` will be the base directory used when recursively walking the file structure in search of asset files. We also declare `pkg` which will be the package name used in the asset file. The importance of this may be to have have multiple asset directories within a project but in separate directory structures.

``` go "handling arguments" +=

directory, _ := filepath.Abs(os.Args[1])
pkg := strings.Replace(directory, path.Dir(directory)+"/", "", 1)

```

which of course needs some more packages

``` go "imports" +=
"path"
"path/filepath"
"strings"
```

All remaining arguments represent the allowed extensions associated with assets.

``` go "handling arguments" +=

extensions := []string{}
for _, extension := range os.Args[2:] {
	extensions = append(extensions, extension)
}

```

For example, if you wanted to only include JSON and PNG then you could run the program as `embed . .json .png`. A default set of extensions are provided with common asset file types just in case none were provided:

``` go "handling arguments" +=

// If no extensions were provided then add a default list
if len(extensions) == 0 {
	extensions = []string{
		".png", ".bmp", ".jpg", ".jpeg", ".gif",
		".wav", ".mp3", ".ogg",
		".mp4", ".mpg", ".mpeg",
	}
}

```

## Collecting assets within the directory

Since we currently have a set of extensions to look for and a base directory set to search through we can walk through the directory and only add file paths which meet our criteria.

``` go "collecting assets"

// Walk through the directory looking for matching file extensions
filepaths := []string{}
err := filepath.Walk(directory, func(fp string, info os.FileInfo, err error) error {
	if err != nil {
		fmt.Printf("Unable to access path %q: %v\n", fp, err)
		return err
	}

	if !info.IsDir() && contains(path.Ext(fp), extensions) {
		filepaths = append(filepaths, fp)
	}

	return nil
})
if err != nil {
	fmt.Printf("Error walking directory %q: %v\n", directory, err)
	return
}

```

The `contains()` function is a simple lookup to see whether the `string` is found within an array of strings.

``` go "functions" +=

func contains(a string, list []string) bool {
	for _, b := range list {
		if a == b {
			return true
		}
	}
	return false
}

```

## Create the assets file

The basic flow for creating the assets file will follow these steps:

``` go "create assets file"
<<<create the file>>>
<<<header information>>>
<<<file content>>>
<<<footer information>>>
```

### Create the file

A new file will be create at the base directory called `assets.go`. If the file already exists then this will overwrite the file!

``` go "create the file"

out, err := os.OpenFile(filepath.Join(directory, "assets.go"), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
if err != nil {
	fmt.Printf("Failed to creating file: %v", err)
	return
}
defer out.Close()

```

Of course we defer closing the file until we have completed all writing operations.

### Asset file header

Writing the header information we will simply call another function which will handle all of the dirty bits for us.

``` go "header information"
header(out, pkg)
```

The header (and subsequent `store()` and `footer()` functions) will accept an `io.Writer` interface so we need to import the package:

``` go "imports" +=
"io"
```

The `header()` function simply outputs to the `io.Writer` and passes in some usages of the package name.

``` go "header"

func header(out io.Writer, pkg string) {
	fmt.Fprintf(out, `
<<<header content>>>
`, pkg, pkg)
}

```

Here is the core content of the header which includes helper functions used in accessing the embedded assets

``` go "header content"
package %s

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
)

<<<reading assets>>>

<<<unpacking assets>>>

<<<filtering assets>>>

var bag = map[string][]byte{
```

#### Unpacking assets

One nice feature is the ability to unpack embedded assets and store them locally relative to the programs location. This is useful for

 - debugging
 - modding
 
If the assets have already been unpacked then it will not overwrite any content but will unpack anything which is determined missing.

``` go "unpacking assets"
// Unpack all assets in to the current directory.
func Unpack() {
	dir := "%s"
	if _, err := os.Stat(dir); err != nil && os.IsNotExist(err) {
		os.Mkdir(dir, os.ModePerm)
	}

	for file, contents := range bag {
		if _, err := os.Stat(file); err != nil && os.IsNotExist(err) {
			out, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE, 0644)
			if err != nil {
				log.Printf("Failed to unpack file: %%s", err)
			}
			out.Write(contents)
			out.Close()
		}
	}
}
```

#### Reading assets

It will be necessary to read our embedded assets. There is a concept of `cached` which refers to extracting the asset using the internally stored information found in our `var bag = map[string][]byte{}`. If it cannot be found within one of our embedded assets then we attempt to read from the file system. Of course we can bypass all of this and just read from the file system directly.

``` go "reading assets"
// Read the asset.
// If cache is enabled then it attempts to pull from the embed
// contents first and using the file as a fallback.
func Read(filename string, cached bool) ([]byte, error) {
	if cached {
		if contents, ok := bag[filename]; ok {
			return contents, nil
		}
	}
	return ioutil.ReadFile(filename)
}
```

#### Filtering assets

Typical when developing games it is important to preload some assets before the game actually begins. For instance you may with to cache all textures which have an extensions of `.png` or `.jpeg`. This function helps to make this more accessible by default.

``` go "filtering assets"
// WithSuffices returns an array of asset names with matching suffices.
func WithSuffices(suffices ...string) []string {
	filenames := []string{}
	for _, suffix := range suffices {
		for filename, _ := range bag {
			if strings.HasSuffix(filename, suffix) {
				filenames = append(filenames, filename)
			}
		}
	}
	return filenames
}
```

### Asset file content

For each file path we read the file contents in and write it out to the assets artifact.

``` go "file content"
for _, fp := range filepaths {
	contents, err := ioutil.ReadFile(fp)
	if err != nil {
		fmt.Printf("Failed to read file: %s", err)
		continue
	}

	file := strings.Replace(fp, directory+"/", "", 1)
	store(out, pkg, file, contents)
}
```

To read the entire file contents we use `ioutil.ReadFile()` so need to import another package:

``` go "imports" +=
"io/ioutil"
```

The storage of the file's binary content is pretty straight forward, but we do complicate things to make it more presentable to the human reader with some basic formatting (blocks of 12 separated by spaces).

``` go "store"

func store(out io.Writer, pkg, file string, contents []byte) {
	out.Write([]byte{'\t'})
	fmt.Fprintf(out, `"%s": []byte{`, filepath.Join(pkg, file))

	count := 0
	for n := range contents {
		if count%12 == 0 {
			out.Write([]byte{'\n'})
			out.Write([]byte{'\t', '\t'})
			count = 0
		} else {
			out.Write([]byte{' '})
		}

		fmt.Fprintf(out, "0x%02x,", contents[n])
		count++
	}

	out.Write([]byte{'\n'})
	out.Write([]byte{'\t'})
	fmt.Fprintf(out, `}, `)
	out.Write([]byte{'\n'})
}

```

### Asset file footer

The footer is meant to wrap-up any asset file template which in this case is a simple closing curly brace.

``` go "footer"

func footer(out io.Writer) {
	fmt.Fprintf(out, `}`)
}

```

Finally we output the footer information to the asset file

``` go "footer information"
footer(out)
```

## File structure

``` go main.go
package main

import (
    <<<imports>>>
)

func main() {
    <<<handling arguments>>>
    <<<collecting assets>>>
    <<<create assets file>>>
}

<<<functions>>>
```

where functions may be further broken down in to

``` go "functions" +=
<<<header>>>
<<<store>>>
<<<footer>>>
```

