package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Not enough arguments provided")
		return
	}

	directory, _ := filepath.Abs(os.Args[1])
	pkg := strings.Replace(directory, path.Dir(directory)+"/", "", 1)

	extensions := []string{}
	for _, extension := range os.Args[2:] {
		extensions = append(extensions, extension)
	}

	// If no extensions were provided then add a default list
	if len(extensions) == 0 {
		extensions = []string{
			".png", ".bmp", ".jpg", ".jpeg", ".gif",
			".wav", ".mp3", ".ogg",
			".mp4", ".mpg", ".mpeg",
		}
	}

	// Walk through the directory looking for matching file extensions
	filepaths := []string{}
	err := filepath.Walk(directory, func(fp string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("Unable to access path %q: %v\n", fp, err)
			return err
		}

		if !info.IsDir() && contains(path.Ext(fp), extensions) {
			filepaths = append(filepaths, fp)
		}

		return nil
	})
	if err != nil {
		fmt.Printf("Error walking directory %q: %v\n", directory, err)
		return
	}

	out, err := os.OpenFile(filepath.Join(directory, "assets.go"), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		fmt.Printf("Failed to creating file: %v", err)
		return
	}
	defer out.Close()

	header(out, pkg)
	for _, fp := range filepaths {
		contents, err := ioutil.ReadFile(fp)
		if err != nil {
			fmt.Printf("Failed to read file: %s", err)
			continue
		}

		file := strings.Replace(fp, directory+"/", "", 1)
		store(out, pkg, file, contents)
	}
	footer(out)
}

func contains(a string, list []string) bool {
	for _, b := range list {
		if a == b {
			return true
		}
	}
	return false
}

func header(out io.Writer, pkg string) {
	fmt.Fprintf(out, `
package %s

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// Read the asset.
// If cache is enabled then it attempts to pull from the embed
// contents first and using the file as a fallback.
func Read(filename string, cached bool) ([]byte, error) {
	if cached {
		if contents, ok := bag[filename]; ok {
			return contents, nil
		}
	}
	return ioutil.ReadFile(filename)
}

// Unpack all assets in to the current directory.
func Unpack() {
	dir := "%s"
	if _, err := os.Stat(dir); err != nil && os.IsNotExist(err) {
		os.Mkdir(dir, os.ModePerm)
	}

	for file, contents := range bag {
		if _, err := os.Stat(file); err != nil && os.IsNotExist(err) {
			out, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE, 0644)
			if err != nil {
				log.Printf("Failed to unpack file: %%s", err)
			}
			out.Write(contents)
			out.Close()
		}
	}
}

// WithSuffices returns an array of asset names with matching suffices.
func WithSuffices(suffices ...string) []string {
	filenames := []string{}
	for _, suffix := range suffices {
		for filename, _ := range bag {
			if strings.HasSuffix(filename, suffix) {
				filenames = append(filenames, filename)
			}
		}
	}
	return filenames
}

var bag = map[string][]byte{
`, pkg, pkg)
}

func store(out io.Writer, pkg, file string, contents []byte) {
	out.Write([]byte{'\t'})
	fmt.Fprintf(out, `"%s": []byte{`, filepath.Join(pkg, file))

	count := 0
	for n := range contents {
		if count%12 == 0 {
			out.Write([]byte{'\n'})
			out.Write([]byte{'\t', '\t'})
			count = 0
		} else {
			out.Write([]byte{' '})
		}

		fmt.Fprintf(out, "0x%02x,", contents[n])
		count++
	}

	out.Write([]byte{'\n'})
	out.Write([]byte{'\t'})
	fmt.Fprintf(out, `}, `)
	out.Write([]byte{'\n'})
}

func footer(out io.Writer) {
	fmt.Fprintf(out, `}`)
}
